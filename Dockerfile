FROM alpine:latest
COPY ./target/x86_64-unknown-linux-musl/release/gitlab-project-doctor /usr/local/bin/gitlab-project-doctor
CMD ["/usr/local/bin/gitlab-project-doctor"]